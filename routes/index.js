var express = require('express');
var router = express.Router();

var mqttHandler = require('../api-resources/mqtt_handler');

//start mqtt client
var mqttClient = new mqttHandler();
mqttClient.connect();

router.post('/send-mqtt', function(req, res) {
  mqttClient.sendMessage(req.body.identifier,req.body.message);
  res.status(200).send("Message sent to mqtt");
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
