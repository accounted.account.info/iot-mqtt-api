import * as express from 'express';
import * as http from 'http';
import * as WebSocket from 'ws';
import * as mqtt from 'mqtt';

const mqttServer  = 'mqtt://localhost:1883';
const app = express();


//initialize a simple http server
const server = http.createServer(app);

//initialize the WebSocket server instance
const wss = new WebSocket.Server({ server });

wss.on('connection', (ws: WebSocket) => {

    const client  = mqtt.connect(mqttServer);
    //connection is up, let's add a simple simple event
    ws.on('message', (message: string) => {

        //log the received message and send it back to the client
        console.log('received: %s', message); 
        const data = JSON.parse(message);       
        ws.send(JSON.stringify({source:"backend",content:`${message}`}));
        client.publish(data.source, data.content)
    });

    //send immediatly a feedback to the incoming connection    
    ws.send(JSON.stringify({source:"backend",content:`Hi there, I am a WebSocket server`}));

    client.on('connect', function () {
        client.subscribe('#', function (err) {
          if (!err) {
            ws.send(JSON.stringify({source:"backend",content:`mqtt connected and subscribed to #`}));
          } else {
            ws.send(JSON.stringify({source:"backend",content:`mqtt cannot connect`})); 
          }
        })
      })
      
    client.on('message', function (topic, message) {
        // message is Buffer
        console.log(message.toString())
        ws.send(JSON.stringify({source:topic,content:`${message}`}));
    })
});


//start our server
server.listen(process.env.PORT || 8999, () => {
    console.log(`Server started on port :${process.env.PORT || 8999} `);
});

